package inv.project.dao;

import inv.project.model.Persona;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PersonaDaoImp implements PersonaDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Integer save(Persona persona) {
		sessionFactory.getCurrentSession().save(persona);
		return persona.getId();
	}

	public Persona get(Integer id) {
		return sessionFactory.getCurrentSession().get(Persona.class, id);
	}

	public List<Persona> list() {
		// TODO Auto-generated method stub
		return null;
	}

	public void update(Integer id, Persona persona) {
		Session session = sessionFactory.getCurrentSession();
		Persona personaActualizada = session.byId(Persona.class).load(id);
		personaActualizada.setNombre(persona.getNombre());
		personaActualizada.setApellido(persona.getApellido());
		session.flush();
	}

	public void delete(Integer id) {
		Session session = sessionFactory.getCurrentSession();
		Persona persona = session.byId(Persona.class).load(id);
		session.delete(persona);
	}

}
